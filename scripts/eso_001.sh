#!/bin/sh
DATE=`date +%m-%d-%Y`
NC='\033[0m'
GRN='\e[32m'

while [ "$n" != "0" ]
do
printf "${GRN}-------------------------------------------------------------------${NC}\n"
echo   "     |------------Gestion local de l'application-mgt------------|"
printf "${GRN}-------------------------------------------------------------------${NC}\n"

printf "${GRN}To day :${NC} ${DATE}${NC}\n"    
echo "[ ------------------------------- ]"
echo "1: Full check status application"
echo "2: Stop application"
echo "3: Start application"
echo "4: Restart application "
echo "0: Exit "
#read options
echo ">> :"
read n

case $n in
        1) echo "${GRN}Check status application${NC}\n" ;;
        2) echo "Stop app " ;;
        3) sh /jenkins/slave/build/workspace/livraison-gitlab-project_hakim/scripts/start-app.sh > /dev/null 2>&1  ;;
        4) echo "Restart";;
        *) echo "Option incorrect";;
        0) echo "Exit";;
esac
  echo "[-----------------------------------------]"
done